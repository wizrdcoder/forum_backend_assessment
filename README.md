# Blog Backend API (Django)

## Setup

The first thing to do is to clone the repository:

```sh
$ git clone https://wizrdcoder@bitbucket.org/wizrdcoder/forum_backend_assessment.git
$ cd forum_backend_assessment
```

Create a virtual environment to install dependencies in and activate it:

```sh
$ virtualenv --no-site-packages env
$ source env/bin/activate
```

Then install the dependencies:

```sh
(env)$ pip install -r ps_blog_backend/requirements.txt
```
Note the `(env)` in front of the prompt. This indicates that this terminal
session operates in a virtual environment set up by `virtualenv`.

Once `pip` has finished downloading the dependencies:
```sh
(env)$ cd ps_blog_backend
(env)$ python manage.py runserver
```
And navigate to `http://127.0.0.1:8000/`.

## Walkthrough - API endpoints

This application has 5 endpoints
* Register -> (POST) localhost:8000/account/register 
* Login -> (POST) localhost:8000/account/register 

* New post -> (POST) localhost:8000/post/new 
* All Posts -> (GET) localhost:8000/post 
* Update Post -> (PATCH) localhost:8000/post/(id)/update 
* Delete Post -> (DELETE) localhost:8000/post/(id)/delete 
* Favourite(Like/Dislike) Post (POST) -> localhost:8000/post/id/(is_fav) 

## TODO
`Write Test cases`
`Email validation`
`Implement authorization`