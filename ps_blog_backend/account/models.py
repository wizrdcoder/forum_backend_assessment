from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

ACCOUNT_TYPE = (('admin', 'Admin'), ('user', 'User'))


class UserManager(BaseUserManager):
    def create_user(self,username, email, password=None):
        if not email:
            raise ValueError('User email must be specified')

        user = self.model(
            email=self.normalize_email(email), username=username
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        super_user = self.create_user(username,email,password)
        super_user.is_staff = True
        super_user.is_superuser = True
        super_user.is_active = True
        super_user.save(using=self._db)
        return super_user


class User(AbstractBaseUser, PermissionsMixin):
    class AccountType:
        ADMIN = ACCOUNT_TYPE[0][0]
        USER = ACCOUNT_TYPE[1][0]

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ['email']
    email = models.EmailField(_('email address'), max_length=255, unique=True, db_index=True)
    username = models.CharField(_('Username'), max_length=255, unique=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))

    is_superuser = models.BooleanField(_('super status'), default=False,
                                       help_text=_('Designates whether the user can log into this admin '
                                                   'site.'))

    is_active = models.BooleanField(_('active'), default=False,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()