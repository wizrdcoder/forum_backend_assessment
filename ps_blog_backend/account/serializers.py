from rest_framework import serializers
from django.db import transaction
from .models import User


class RegisterUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255, write_only=False)
    password = serializers.CharField(max_length=1000, write_only=True)
    username = serializers.CharField(max_length=255, write_only=False)
    first_name = serializers.CharField(max_length=255, write_only=False)
    last_name = serializers.CharField(max_length=255, write_only=False)

    def validate_email(self, email):
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError("Email already exists")
        return email

    def validate_username(self, username):
        if User.objects.filter(username=username).exists():
            raise serializers.ValidationError("This username has already been taken")
        return username

    def create(self, validated_data):
        with transaction.atomic():
            first_name = validated_data.get("first_name", "")
            last_name = validated_data.get("last_name", "")

            user = User.objects.create(
                username=validated_data.get("username"),
                email=validated_data.get("email"),
                first_name=first_name,
                last_name=last_name,
            )
            user.set_password(validated_data.get("password"))
            user.is_active = True
            user.save()

            return user

    class Meta:
        model = User
        fields = '__all__'
