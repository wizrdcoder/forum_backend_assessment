from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from . import api

app_name = 'account'

urlpatterns = [
    path('register', api.SignupUser.as_view(), name='register'),
    path('login', obtain_auth_token, name='login'),
]
