from rest_framework import generics
from . import serializers
from rest_framework.permissions import IsAuthenticated
from .models import Post
from django.shortcuts import get_object_or_404


class NewPostView(generics.CreateAPIView):
    serializer_class = serializers.PostSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class ListPostView(generics.ListAPIView):
    serializer_class = serializers.PostSerializer
    queryset = Post.objects.all()


class UpdatePostView(generics.UpdateAPIView):
    serializer_class = serializers.PostSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        post_id = self.kwargs.get('id')
        post = get_object_or_404(Post, author=self.request.user, id=post_id)
        return post


class DeletePostView(generics.DestroyAPIView):
    serializer_class = serializers.PostSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        post_id = self.kwargs.get('id')
        post = get_object_or_404(Post, author=self.request.user, id=post_id)
        return post

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class TogglePostPreferenceView(generics.CreateAPIView):
    serializer_class = serializers.UserPostPreferenceSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        post_id = self.kwargs.get('post_id')
        post = get_object_or_404(Post, id=post_id)
        return post

    def get_serializer_context(self):
        context = super().get_serializer_context()
        post = get_object_or_404(Post, id=self.get_object().id)
        context.update({
            "post": post,
            "user": self.request.user,
            "is_fav": self.kwargs.get('is_fav')

        })
        return context