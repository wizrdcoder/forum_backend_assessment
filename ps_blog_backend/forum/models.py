from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

User = get_user_model()


class Post(models.Model):
    """ Posts model """

    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='my_posts')
    title = models.CharField(max_length=50, default='untitled', help_text=_('Title of the post'))
    body = models.TextField(help_text=_('Body of the post'))
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.title


class UserPostPreference(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='post_preferences')
    post = models.ForeignKey(Post, on_delete=models.SET_NULL, null=True, blank=True)
    value = models.IntegerField(help_text=_('Designate value for like and dislike (1-> like, 0->dislike)'),
                                null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return str(self.user) + ':' + str(self.post) + ':' + str(self.value)

    class Meta:
        unique_together = ("user", "post", "value")