from rest_framework import serializers
from forum.models import Post, UserPostPreference


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'


class UserPostPreferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPostPreference
        fields = '__all__'

        extra_kwargs = {
            'user': {'read_only': True},
            'post': {'read_only': True},
            'value': {'read_only': True},
        }

    def create(self, validated_data):
        post = self.context.get('post')
        user = self.context.get('user')
        is_fav = self.context.get('is_fav')
        if user.post_preferences.filter(user=user, post=post).exists():
            post_pref = user.post_preferences.get(user=user, post=post)
            post_pref.value = is_fav
            post_pref.save()
            return post_pref
        else:
            favObj = user.post_preferences.create(post=post, user=user, value=is_fav)
            favObj.save()
            return favObj

        return validated_data
