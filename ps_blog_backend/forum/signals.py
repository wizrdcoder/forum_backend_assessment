from .models import Post,UserPostPreference
from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender=UserPostPreference)
def update_post_likes_signal(sender, instance, **kwargs):
    if instance.value == 1:
        instance.post.likes = instance.post.likes + 1
    else:
        instance.post.dislikes = instance.dislikes.likes - 1

    instance.post.save()