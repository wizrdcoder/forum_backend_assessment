from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from . import api

app_name = 'forum'

urlpatterns = [
    path('post/new', api.NewPostView.as_view(), name='create-post'),
    path('post', api.ListPostView.as_view(), name='list-post'),
    path('post/<id>/update', api.UpdatePostView.as_view(), name='update-post'),
    path('post/<id>/delete', api.DeletePostView.as_view(), name='delete-post'),
    path('post/<post_id>/<is_fav>', api.TogglePostPreferenceView.as_view(), name='toggle-post-like'),
]
